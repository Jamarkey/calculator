package com.jdb.ejb.beans.interfaces;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import com.jdb.jpa.entity.Audit;
import com.jdb.jpa.entity.User;

@Local
public interface AuditLocal {
	public Audit writeAudit(Audit audit) throws SQLException;
	public List<Audit> getAllAudits();
	public List<User> getAuditUsers();
	public List<Audit> getAuditFilter(String user, Date dateFrom, Date dateTo);
}
