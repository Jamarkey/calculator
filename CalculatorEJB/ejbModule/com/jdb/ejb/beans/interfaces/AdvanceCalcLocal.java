package com.jdb.ejb.beans.interfaces;

import java.math.BigDecimal;

import javax.ejb.Local;

@Local
public interface AdvanceCalcLocal {
	public BigDecimal calculate(String inputString) throws IllegalArgumentException;
}
