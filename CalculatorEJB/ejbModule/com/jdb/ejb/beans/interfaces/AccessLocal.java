package com.jdb.ejb.beans.interfaces;

import javax.ejb.Local;

import com.jdb.jpa.entity.User;

@Local
public interface AccessLocal {
	
	public User userLogin(User user);
	public User registerUser(User user);
	public User getUser(User user);
	
}
