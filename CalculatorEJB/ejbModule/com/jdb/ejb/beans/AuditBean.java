package com.jdb.ejb.beans;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.jdb.ejb.beans.interfaces.AuditLocal;
import com.jdb.jpa.entity.Audit;
import com.jdb.jpa.entity.User;

@Stateless
public class AuditBean implements AuditLocal {
	@PersistenceContext
	private EntityManager em;

	/**
	 * Default constructor.
	 */
	public AuditBean() {

	}

	@Override
	public Audit writeAudit(Audit audit) throws SQLException {
		System.out.println("Writing to Audit Table" + audit.getAnswer());
		try {
			em.persist(audit);
		} catch (Exception e) {
			throw new SQLException("Failed to write to Audit table " + e.getMessage());
		}
		return audit;
	}

	@Override
	public List<Audit> getAuditFilter(String user, Date dateFrom, Date dateTo) {
		Map<String, Object> paramaterMap = new HashMap<String, Object>();
		StringBuilder queryBuilder = new StringBuilder();
		StringBuilder whereClause = new StringBuilder();
		int i = 0;

		//Build up the Filter query by Parameters supplied from Screen
		queryBuilder.append("select a FROM Audit a ");

		if (user != null && !user.equals("")) {
			i++;
			whereClause.append(" where a.userName = :user ");
			paramaterMap.put("user", user);
		}

		if (dateFrom != null) {
			if (i++ > 0) {
				whereClause.append(" and ");
			} else {
				whereClause.append(" where ");
			}
			paramaterMap.put("dateFrom", dateFrom);
			whereClause.append(" cast(a.requestedDate AS date) <= cast(:dateFrom AS DATE) ");
		}

		if (dateTo != null) {
			if (i++ > 0) {
				whereClause.append(" and ");
			} else {
				whereClause.append(" where ");
			}
			paramaterMap.put("dateTo", dateTo);
			whereClause.append(" cast(a.requestedDate AS date) <= cast(:dateTo AS DATE) ");
		}
		
		queryBuilder.append(whereClause);

		queryBuilder.append("order by a.requestedDate");
		System.out.println("query " + queryBuilder);
		Query query = em.createQuery(queryBuilder.toString());
		//loops through the parameter map and set 
		for (String key : paramaterMap.keySet()) {
			query.setParameter(key, paramaterMap.get(key));
		}
		return query.getResultList();
	}

	@Override
	public List<Audit> getAllAudits() {
		System.out.println("getting all Audit");
		return em.createNamedQuery("Audit.getAllAudit", Audit.class).getResultList();
	}

	@Override
	public List<User> getAuditUsers() {
		return em.createNamedQuery("Audit.getAllAuditUsers", User.class).getResultList();
	}

}
