package com.jdb.ejb.beans;

import java.math.BigDecimal;
import java.sql.SQLException;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.jdb.ejb.beans.interfaces.AdvanceCalcLocal;
import com.jdb.jpa.entity.User;

@Stateless
public class AdvancCalculations implements AdvanceCalcLocal {

	/*
	 * Brackets (parts of a calculation inside brackets always come first). Orders
	 * (numbers involving powers or square roots). Division. Multiplication.
	 * Addition. Subtraction.
	 */

	@Override
	public BigDecimal calculate(String inputString) throws IllegalArgumentException {
		//makes sure to remove all spaces and make all functions/constants lowercase
		inputString = inputString.trim().replace(" ", "").toLowerCase();
		return BigDecimal.valueOf(new Converter(inputString).convert());
	}

	static class Converter {
		// Constants Enum that allows for more constants to be added later with ease.
		private enum Constants {
			PI(Math.PI, "pi"), E(Math.E, "e");

			private double value;
			private String text;

			private Constants(double value, String text) {
				this.value = value;
				this.text = text;
			}

			public double getValue() {
				return value;
			}

			// Search for enum with String and return Enum if found
			public static Constants fromString(String text) {
				for (Constants constants : Constants.values()) {
					if (constants.text.equalsIgnoreCase(text)) {
						return constants;
					}
				}
				throw new IllegalArgumentException("No constant with text " + text + " found");
			}

		}

		// Functions enum so that later functions can be added with ease (currently only
		// supports functions with 2 inputs)
		private enum Functions {
			ROOT("root"), SIN("sin"), COS("cos"), TAN("tan");

			private String text;

			private Functions(String text) {
				this.text = text;
			}
			// Search for enum with String and return Enum if found
			public static Functions fromString(String text) {
				for (Functions constants : Functions.values()) {
					if (constants.text.equalsIgnoreCase(text)) {
						return constants;
					}
				}
				return null;
			}

			// Calls the function based on the Enum supplied
			public double calculate(double x, double y) {
				switch (this) {
				case ROOT:
					return Math.pow(x, 1 / y);
				case SIN:
					return Math.sin(Math.toRadians(x));
				case COS:
					return Math.cos(Math.toRadians(x));
				case TAN:
					return Math.tan(Math.toRadians(x));
				default:
					return 0;
				}
			}
		}

		int pos = -1;
		int charAt; // charAt
		final String str;

		Converter(String str) {
			this.str = str;
		}

		//Checks the current char to to match incoming variable
		private boolean isCorrectChar(int charToMatch) {
			if (charAt == charToMatch) {
				nextChar();
				return true;
			}
			return false;
		}

		//gets the next char in the string
		private void nextChar() {
			charAt = (++pos < str.length()) ? str.charAt(pos) : -1;
		}

		//main convert method that will start the recursive functions to work through the string supplied
		private double convert() {
			nextChar();
			double x = handleExpression();
			return x;
		}

		//Method used to evaluate the expression, by calling the handleTerm method to get the answer for the whole expression
		private double handleExpression() {
			double x = handleTerm(); //first check to see the fist part of the BODMAS rule
			while (true) {
				if (isCorrectChar('+')) { //Check for a plus sign in the expression to handle the addition after all other terms has been evaluated
					x += handleTerm(); 
				} else if (isCorrectChar('-')) { //Check for a Minus sign in the expression to handle the subtraction after all other terms has been evaluated
					x -= handleTerm(); 
				} else {
					return x;
				}
			}
		}
		//Method used to handle specific terms in the expression where it will get the answer after all multiplication/division has been handled.
		private double handleTerm() {
			double x = handleFactor(); //first check to handle the Brackets and orders
			for (;;) {
				if (isCorrectChar('*')) { //Check for a multiplication sign in the expression
					x *= handleFactor(); 
				} else if (isCorrectChar('/')) { //Check for a divide sign in the expression
					x /= handleFactor(); 
				} else {
					return x;
				}
			}
		}
		//The main recursive function that will keep on looping till all the brackets and orders have been completed.
		private double handleFactor() {
			if (isCorrectChar('+')) { //To check for double ++ in expression
				return handleFactor();
			} 
			if (isCorrectChar('-')) {//To check for explicit negative numbers in expression and make the result negative
				return -handleFactor();
			} 

			double x;
			int startPos = this.pos; 
			if (isCorrectChar('(')) { //Checks to see for brackets to get the result of the bracket by calling the handleExpression recursively to evaluate the expression inside the brackets as a expression on its own
				x = handleExpression();
				isCorrectChar(')'); //removes the closing bracket after expressions answer has been calculated
			} else if ((charAt >= '0' && charAt <= '9') || charAt == '.') { // check to see if we have a number.
				x = getNumber(startPos);
			} else if (charAt >= 'a' && charAt <= 'z') { // check for functions or constants
				x = doFunction(startPos);
			} else {
				throw new IllegalArgumentException("Unexpected: " + (char) charAt);
			}
			
			//check for the orders sign, to call the Math.pow function on the calculated result and calling the handleFactor method again to calculate the power expression
			if (isCorrectChar('^')) {
				x = Math.pow(x, handleFactor()); 
			}

			return x;
		}

		//Get the function or constant and return the result
		private double doFunction(int startPos) {
			double x;
			while (charAt >= 'a' && charAt <= 'z') { //loops through the whole "word" to get the full function/constant text
				nextChar();
			}
			String func = str.substring(startPos, this.pos); //with the start and end position found extract the full function/constant string

			Functions f;
			f = Functions.fromString(func); //first check to see if the extracted string is in the functions enum, else look for it in the constants enum
			if (f != null) {
				double y = 0;
				x = handleFactor();// get the 1st (or only) value after the function name and bracket
				if (f.equals(Functions.ROOT)) {//if the function is root we require a seccond value that is seperated by a comma
					isCorrectChar(',');
					y = handleFactor();
				}

				x = f.calculate(x, y); //call the enum's calculate method to calculate the correct function
			} else {
				x = getConstant(func); //get the constants value
			}
			return x;
		}

		private double getConstant(String constant) {
			Constants c;
			c = Constants.fromString(constant); //check to see if the constant string is in the constants enum and return correct enum.
			return c.getValue(); //gets the constants value from the enum
		}

		private double getNumber(int startPos) {
			double x;
			while ((charAt >= '0' && charAt <= '9') || charAt == '.') //loops through the string where a number has been identified to extract the whole number
				nextChar();
			x = Double.parseDouble(str.substring(startPos, this.pos));
			return x;
		}

	}

}
