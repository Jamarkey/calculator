package com.jdb.ejb.beans;

import com.jdb.ejb.beans.interfaces.AccessLocal;
import com.jdb.jpa.entity.User;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

/**
 * Session Bean implementation class UserDAO
 */
@Stateless
public class AccessBean implements AccessLocal {

	@PersistenceContext
	private EntityManager em;

	/**
	 * Default constructor.
	 */
	public AccessBean() {

	}

	@Override
	public User userLogin(User user) {
		return validateUser(user);
	}

	@Override
	public User registerUser(User user) {
		return create(user);
	}

	@Override
	public User getUser(User user) {
		return em.find(User.class, user.getId());
	}

	private String encryptPassword(String password) {
		return encryptWithMD5(password).toString();
	}

	private String encryptWithMD5(String pass) {
		try {
			MessageDigest md;
			md = MessageDigest.getInstance("MD5");
			byte[] passBytes = pass.getBytes();
			md.reset();
			byte[] digested = md.digest(passBytes);
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < digested.length; i++) {
				sb.append(Integer.toHexString(0xff & digested[i]));
			}
			return sb.toString();
		} catch (NoSuchAlgorithmException ex) {
			System.out.println("Could not encrypt password " + ex.getMessage());
		}
		return null;
	}

	private User validateUser(User user) {
		User u = findUserByUserName(user);
		//Make sure the user if found to compare passwords
		if (u == null) {
			return u;
		}

		//compare password with pasword in Database
		if (u.getPassword().equalsIgnoreCase(encryptPassword(user.getPassword()))) {
			return u;
		} else {
			return null;
		}
	}

	private User findUserByUserName(User user) {
		try {
			return em.createNamedQuery("User.findByUsername", User.class).setParameter("username", user.getUsername())
					.getSingleResult();
		} catch (NoResultException nre) {
			return null;
		}
	}

	private User create(User user) {
		User u = findUserByUserName(user);
		//Make sure that the user name does not already exist before creating new User
		if (u != null) {
			return null;
		} else {
			user.setPassword(encryptPassword(user.getPassword()));
			em.persist(user);
			return user;
		}
	}

}
