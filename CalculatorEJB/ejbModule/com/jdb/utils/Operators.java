package com.jdb.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;

//public class e {
public enum Operators {

	ADDITION("+"), SUBTRACTION("-"), MULTIPLICATION("*"), DIVISION("/");

	private String label;

	private Operators(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}

	public String getCalculation(BigDecimal input1, BigDecimal input2) {
		return input1 + this.getLabel() + input2;
	}

	public BigDecimal calculate(BigDecimal input1, BigDecimal input2, int rounding) {
		switch (this) {
		case ADDITION:
			return input1.add(input2);

		case SUBTRACTION:
			return input1.subtract(input2);

		case MULTIPLICATION:
			return input1.multiply(input2);

		case DIVISION:
			if (input2.equals(BigDecimal.ZERO)) {
				return BigDecimal.ZERO;
			}
			return input1.divide(input2, rounding, RoundingMode.HALF_EVEN);

		default:
			return BigDecimal.ZERO;
		}
	}

}

//}
