package com.jdb.jpa.entity;

import java.io.Serializable;
import java.lang.String;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: User
 *
 */
@Entity
@NamedQuery(name = "User.findByUsername", query = "Select u FROM User u where u.username = :username")
@Table(name = "users")
public class User implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int Id;
	private String username;
	private String password;
	private int administrator;
	private static final long serialVersionUID = 1L;

	public User() {
		super();
	}

	public int getId() {
		return this.Id;
	}

	public void setId(int Id) {
		this.Id = Id;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getAdministrator() {
		return this.administrator;
	}

	public void setAdministrator(int administrator) {
		this.administrator = administrator;
	}

}
