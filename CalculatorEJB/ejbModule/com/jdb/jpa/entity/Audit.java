package com.jdb.jpa.entity;

import java.io.Serializable;
import java.lang.String;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Audit
 *
 */
@Entity
@NamedQueries({ @NamedQuery(name = "Audit.getAllAudit", query = "Select a FROM Audit a order by a.requestedDate"),
		@NamedQuery(name = "Audit.getAllAuditUsers", query = "Select distinct a.userName FROM Audit a") })
@Table(name = "Audit")
public class Audit implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String calculation;
	private BigDecimal answer;
	private String userName;
	private int rounding;
	private Timestamp requestedDate;
	private static final long serialVersionUID = 1L;

	public Audit() {
		super();
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCalculation() {
		return this.calculation;
	}

	public void setCalculation(String Calculation) {
		this.calculation = Calculation;
	}

	public BigDecimal getAnswer() {
		return this.answer;
	}

	public void setAnswer(BigDecimal Answer) {
		this.answer = Answer;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Timestamp getRequestedDate() {
		return this.requestedDate;
	}

	public void setRequestedDate(Timestamp requestedDate) {
		this.requestedDate = requestedDate;
	}

	public int getRounding() {
		return rounding;
	}

	public void setRounding(int rounding) {
		this.rounding = rounding;
	}

}
