package com.jdb.managedbeans;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.util.Date;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.application.ResourceHandler;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.jdb.ejb.beans.interfaces.AuditLocal;
import com.jdb.jpa.entity.Audit;
import com.jdb.jpa.entity.User;
import com.jdb.utils.Operators;

@ViewScoped
@ManagedBean(name = "basic", eager = true)
public class BasicCalcMB {

	@EJB
	AuditLocal auditLocal;

	private Operators operator;
	private BigDecimal inputNumber1;
	private BigDecimal inputNumber2;
	private BigDecimal result;
	private int round = 2;

	public void calculate() {
		if (inputNumber1 == null) {
			addErrorMessage("Please enter a value for input 1");
			return;
		}
		if (inputNumber2 == null) {
			addErrorMessage("Please enter a value for input 2");
			return;
		}
		result = operator.calculate(inputNumber1, inputNumber2, round).setScale(round, RoundingMode.HALF_EVEN);
		Audit audit = new Audit();
		audit.setAnswer(result);
		audit.setCalculation(operator.getCalculation(inputNumber1, inputNumber2));
		audit.setRequestedDate(new java.sql.Timestamp(new Date().getTime()));
		audit.setUserName(getUserName());
		audit.setRounding(round);
		try {
			auditLocal.writeAudit(audit);
		} catch (SQLException se) {
			addErrorMessage("Failed to Write to Audit " + se.getMessage());
		}
	}

	private String getUserName() {
		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User) context.getExternalContext().getSessionMap().get("user");
		System.out.println("user id = " + user.getId());
		return user.getUsername();
	}

	private void addErrorMessage(String message) {
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(message));
	}

	public Operators[] getOperators() {
		return Operators.values();
	}

	public BigDecimal getInputNumber1() {
		return inputNumber1;
	}

	public void setInputNumber1(BigDecimal inputNumber1) {
		this.inputNumber1 = inputNumber1;
	}

	public BigDecimal getInputNumber2() {
		return inputNumber2;
	}

	public void setInputNumber2(BigDecimal inputNumber2) {
		this.inputNumber2 = inputNumber2;
	}

	public BigDecimal getResult() {
		return result;
	}

	public void setResult(BigDecimal result) {
		this.result = result;
	}

	public Operators getOperator() {
		return operator;
	}

	public void setOperator(Operators operator) {
		this.operator = operator;
	}

	public int getRound() {
		return round;
	}

	public void setRound(int round) {
		this.round = round;
	}
}
