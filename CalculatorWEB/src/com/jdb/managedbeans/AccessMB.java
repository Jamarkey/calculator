package com.jdb.managedbeans;

import java.io.IOException;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;

import com.jdb.ejb.beans.interfaces.AccessLocal;
import com.jdb.jpa.entity.User;

@ViewScoped
@ManagedBean(name = "access", eager = true)
public class AccessMB {
	@EJB
	private AccessLocal accessLocal;

	private String userName;
	private String password;
	private boolean administrator;

	public void login() {
		User user = new User();
		user.setPassword(password);
		user.setUsername(userName);
		System.out.println(user);
		user = accessLocal.userLogin(user);
		if (user != null) {
			System.out.println("user id :" + user.getId());
			setUserInSession(user);
			forwardToPage("basicCalc.xhtml");
		} else {
			System.out.println("user credentials does not match");
			addErrorMessage("Username or Password does not match");
		}
	}

	public void registerUser() {
		User user = new User();
		user.setPassword(password);
		user.setUsername(userName);
		user.setAdministrator(administrator ? 1 : 0);
		System.out.println(user);
		user = accessLocal.registerUser(user);
		if (user != null) {
			user.setPassword(password);
			user.setUsername(userName);
			user.setAdministrator(administrator ? 1 : 0);
			System.out.println(user);

			setUserInSession(user);
			forwardToPage("basicCalc.xhtml");
		} else {
			addErrorMessage("User Already Exists");
		}
	}

	public boolean isLoggedIn() {
		return getUser() != null;
	}

	public boolean isAdmin() {
		User u = getUser();
		if (u != null && u.getAdministrator() == 1) {
			return true;
		} 
		return false;
	}

	private User getUser() {
		FacesContext context = FacesContext.getCurrentInstance();
		return (User) context.getExternalContext().getSessionMap().get("user");
	}

	private void setUserInSession(User user) {
		FacesContext context = FacesContext.getCurrentInstance();
		context.getExternalContext().getSessionMap().put("user", user);
	}

	private void addErrorMessage(String message) {
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(message));
	}

	public void logout() {
		System.out.println("Logout Command Called");
		FacesContext context = FacesContext.getCurrentInstance();
		context.getExternalContext().invalidateSession();
		forwardToPage("index.xhtml");
	}

	public void forwardToPage(String page) {
		FacesContext context = FacesContext.getCurrentInstance();
		if (!page.contains(".")) {
			page = page + ".xhtml";
		}
		try {
			context.getExternalContext().redirect(page);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public String forwardToRegister() {
		return "registerUser";
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean getAdministrator() {
		return administrator;
	}

	public void setAdministrator(boolean administrator) {
		this.administrator = administrator;
	}

}
