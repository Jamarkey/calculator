package com.jdb.managedbeans;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.UIViewRoot;
import javax.faces.component.html.HtmlInputText;
import javax.faces.component.html.HtmlSelectBooleanCheckbox;
import javax.faces.component.html.HtmlSelectManyCheckbox;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.FacesContext;

import com.jdb.ejb.beans.interfaces.AuditLocal;
import com.jdb.jpa.entity.Audit;
import com.jdb.jpa.entity.User;
import com.sun.org.apache.xerces.internal.impl.dv.xs.DateDV;

@ViewScoped
@ManagedBean(name = "audit", eager = true)
public class AuditMB {
	@EJB
	AuditLocal auditLocal;

	private List<Audit> audits;
	private String user;
	private String dateFrom;
	private String dateTo;

	public List<Audit> getAllAudit() {
		audits = auditLocal.getAllAudits();
		return audits;
	}

	public List<Audit> getAuditFilter() {
		Date toDate = convertStringToDate(dateTo);
		Date fromDate = convertStringToDate(dateFrom);;

		audits = auditLocal.getAuditFilter(user, fromDate, toDate);
		return audits;
	}

	public List<User> getAuditUsers() {
		return auditLocal.getAuditUsers();
	}

	public String convertAnswer(BigDecimal val) {
		return val.stripTrailingZeros().toPlainString();
	}

	private Date convertStringToDate(String date) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
		Date returnDate = null;
		if (date != null && !date.equals("") && !date.equalsIgnoreCase("null")) {
			try {
				returnDate = sdf.parse(date);
			} catch (ParseException pe) {
				System.out.println("could not parse date " + date + " " + pe.getMessage());
			}
		}
		return returnDate;
	}

	public List<Audit> getAudits() {
		return audits;
	}

	public void setAudits(List<Audit> audits) {
		this.audits = audits;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}

	public String getDateTo() {
		return dateTo;
	}

	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}
}
