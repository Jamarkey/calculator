package com.jdb.managedbeans;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.util.Date;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.jdb.ejb.beans.interfaces.AdvanceCalcLocal;
import com.jdb.ejb.beans.interfaces.AuditLocal;
import com.jdb.jpa.entity.Audit;
import com.jdb.jpa.entity.User;

@ViewScoped
@ManagedBean(name = "advance", eager = true)
public class AdvanceCalcMB {

	@EJB
	AdvanceCalcLocal advanceCalcLocal;
	@EJB
	AuditLocal auditLocal;

	private String input;
	private BigDecimal result;
	private int round = 2;

	public void calculate() {
		if (input == null || input.equalsIgnoreCase("") || input.equalsIgnoreCase("null")) {
			addErrorMessage("Please enter values");
			return;
		}
		try {
			result = advanceCalcLocal.calculate(input).setScale(round, RoundingMode.HALF_EVEN);
			System.out.println("Result " + result);
			Audit audit = new Audit();
			audit.setAnswer(result);
			audit.setCalculation(input);
			audit.setRequestedDate(new java.sql.Timestamp(new Date().getTime()));
			audit.setUserName(getUserName());
			audit.setRounding(round);
			auditLocal.writeAudit(audit);
		} catch (IllegalArgumentException iae) {
			addErrorMessage(iae.getMessage());
			System.err.println("IllegalArgumentException " + iae.getMessage());
		} catch (SQLException se) {
			addErrorMessage(se.getMessage());
			System.err.println("SQLException " + se.getMessage());
		} catch (Exception e) {
			addErrorMessage(e.getMessage());
			System.err.println("Exception " + e.getMessage());
		}

	}

	private String getUserName() {
		FacesContext context = FacesContext.getCurrentInstance();
		User user = (User) context.getExternalContext().getSessionMap().get("user");
		return user.getUsername();
	}

	private void addErrorMessage(String message) {
		FacesContext context = FacesContext.getCurrentInstance();
		context.addMessage(null, new FacesMessage(message));
	}

	public String getInput() {
		return input;
	}

	public void setInput(String input) {
		this.input = input;
	}

	public BigDecimal getResult() {
		return result;
	}

	public void setResult(BigDecimal result) {
		this.result = result;
	}

	public int getRound() {
		return round;
	}

	public void setRound(int round) {
		this.round = round;
	}
}
