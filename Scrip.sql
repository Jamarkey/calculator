drop database calculator;
create database calculator;

drop user app_user;
create user app_user identified by 'Password';

GRANT ALL privileges ON `calculator`.* TO 'app_user'@localhost;

alter user 'app_user'@localhost identified by 'Password';


# drop table users;
create table calculator.users
(
	id int not null AUTO_INCREMENT,
	username varchar(20) null unique ,
	password varchar(100) null,
	administrator int null,
	PRIMARY KEY (`id`)
);

# drop table audit;
create table calculator.audit
(
	id int not null AUTO_INCREMENT ,
	calculation varchar(200) not null ,
	answer decimal(15,4) not null ,
	rounding int not null,
	userName varchar(20) not null ,
	requestedDate timestamp,
	PRIMARY KEY (`id`)
);